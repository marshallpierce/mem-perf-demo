FROM rust:1.50.0

RUN apt-get update && apt install -y linux-perf

RUN rustup toolchain add nightly
