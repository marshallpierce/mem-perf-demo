use crate::WordCounter;

use std::{fmt::Debug, fs, io, io::BufRead, io::Read, path};

/// Implementation that reads each line into a `String`, and splits each line into per-word `Strings`
#[derive(Debug)]
pub(crate) struct LineSplitStringBufReaderCounter;

impl WordCounter for LineSplitStringBufReaderCounter {
    fn count_words(&self, path: &path::Path) -> io::Result<u64> {
        let file = fs::File::open(path)?;
        // Copies data into internal buffer
        let buffered = io::BufReader::new(file);

        let mut count: u64 = 0;

        // Copies data from BufReader buffer into String
        for line_res in buffered.lines() {
            let line = line_res?;

            // Mimic how Java's split works: produces `String[]` (or in Rust, `Vec<String>`)
            let words = line
                .split(' ')
                // Copy data from line String into per-word String
                .map(|word_str| word_str.to_owned())
                // fill collection
                .collect::<Vec<String>>();

            count += words.len() as u64;
        }

        Ok(count)
    }
}

/// Reads each line into a `String`, then splits each line into `&str`s
#[derive(Debug)]
pub(crate) struct LineSplitStrBufReaderCounter;

impl WordCounter for LineSplitStrBufReaderCounter {
    fn count_words(&self, path: &path::Path) -> io::Result<u64> {
        let file = fs::File::open(path)?;
        // Copies data into internal buffer
        let buffered = io::BufReader::new(file);

        let mut count: u64 = 0;

        // Copies data from BufReader buffer into String
        for line_res in buffered.lines() {
            let line = line_res?;

            // split produces &str - views into the existing String's buffer, no additional copy
            count += line.split(' ').count() as u64;
        }

        Ok(count)
    }
}

/// Like [LineSplitStrBufReaderCounter], but re-uses the same String instance for each line
#[derive(Debug)]
pub(crate) struct ReuseLineStringSplitStrBufReaderCounter;

impl WordCounter for ReuseLineStringSplitStrBufReaderCounter {
    fn count_words(&self, path: &path::Path) -> io::Result<u64> {
        let file = fs::File::open(path)?;
        // Copies data into internal buffer
        let mut buffered = io::BufReader::new(file);

        let mut line = String::with_capacity(1024);

        let mut count = 0;
        loop {
            line.clear();

            // still copying the line, but at least not allocating a new line each time
            let len = buffered.read_line(&mut line)?;

            if len == 0 {
                break;
            }

            count += line.split(' ').count() as u64;
        }

        Ok(count)
    }
}

/// Reads bytes individually from a BufReader to avoid copies
#[derive(Debug)]
pub(crate) struct BufReaderForLoopCounter;

impl WordCounter for BufReaderForLoopCounter {
    fn count_words(&self, path: &path::Path) -> io::Result<u64> {
        let file = fs::File::open(path)?;
        // Copies data into internal buffer
        let buffered = io::BufReader::new(file);

        let mut count = 0_u64;
        // spends a lot of time doing i/o error checks per byte
        for res in buffered.bytes() {
            let byte = res?;

            if byte == b' ' || byte == b'\n' {
                count += 1;
            }
        }

        Ok(count)
    }
}

/// Reads bytes _without_ a BufReader into a Vec<u8> and scans the vec without utf8 validation
#[derive(Debug)]
pub(crate) struct VecForLoopCounter;

impl WordCounter for VecForLoopCounter {
    fn count_words(&self, path: &path::Path) -> io::Result<u64> {
        let mut file = fs::File::open(path)?;
        let mut buf = vec![0; 10 * 1024];

        let mut count = 0_u64;

        loop {
            let len = file.read(&mut buf[..])?;
            if len == 0 {
                break;
            }

            for &byte in &buf[..len] {
                // `if` may stress the branch predictor / speculative execution
                if byte == b' ' || byte == b'\n' {
                    count += 1
                }
            }
        }

        Ok(count)
    }
}

/// Similar to [VecForLoopCounter], but iterating over bytes with `map()` to eliminate branches
#[derive(Debug)]
pub(crate) struct VecMapSumCounter;

impl WordCounter for VecMapSumCounter {
    fn count_words(&self, path: &path::Path) -> io::Result<u64> {
        let mut file = fs::File::open(path)?;
        let mut buf = vec![0; 10 * 1024];

        let mut count = 0_u64;

        loop {
            let len = file.read(&mut buf[..])?;
            if len == 0 {
                break;
            }

            count += buf[0..len]
                .iter()
                // no `if`, same instructions every time
                .map(|&byte| (byte == b' ' || byte == b'\n') as u64)
                .sum::<u64>()
        }

        Ok(count)
    }
}

/// Similar to [VecMapSumCounter], but more idiomatic with `filter()` and `count()`
#[derive(Debug)]
pub(crate) struct VecFilterCountCounter;

impl WordCounter for VecFilterCountCounter {
    fn count_words(&self, path: &path::Path) -> io::Result<u64> {
        let mut file = fs::File::open(path)?;
        let mut buf = vec![0; 10 * 1024];

        let mut count = 0_u64;

        loop {
            let len = file.read(&mut buf[..])?;
            if len == 0 {
                break;
            }

            count += buf[0..len]
                .iter()
                // filter() produces a [Filter], which implements `count()` in a branch free way
                .filter(|&&byte| byte == b' ' || byte == b'\n')
                .count() as u64
        }

        Ok(count)
    }
}
