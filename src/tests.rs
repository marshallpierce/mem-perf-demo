use crate::all_counters;
use std::{error, path};

#[test]
fn test_word_count() -> Result<(), Box<dyn error::Error>> {
    let path: path::PathBuf = "tests/words.txt".into();

    for counter in all_counters() {
        let count = counter.count_words(&path)?;

        assert_eq!(9, count, "counter {:?}", counter);
    }

    Ok(())
}
