use std::{env, fmt::Debug, fs, io, io::Read, iter, os::unix::fs::PermissionsExt, path, time};

use anyhow::Context as _;
use cfg_if::cfg_if;

mod io_read;

use io_read::*;

mod mmap;

use mmap::*;

mod simd;

mod perf_counter;

#[cfg(test)]
mod tests;

trait WordCounter: Debug {
    fn count_words(&self, path: &path::Path) -> io::Result<u64>;
}

fn all_counters() -> Vec<Box<dyn WordCounter>> {
    #[allow(unused_mut)]
    let mut impls: Vec<Box<dyn WordCounter>> = vec![
        Box::new(LineSplitStringBufReaderCounter),
        Box::new(LineSplitStrBufReaderCounter),
        Box::new(ReuseLineStringSplitStrBufReaderCounter),
        Box::new(BufReaderForLoopCounter),
        Box::new(VecForLoopCounter),
        Box::new(VecMapSumCounter),
        Box::new(VecFilterCountCounter),
        Box::new(MmapCounter::new(ByteIterFilterCountCore)),
        Box::new(MmapParallelCounter::new(ByteIterFilterCountCore)),
        Box::new(MmapCounter::new(ByteIterFilterMoreWhitespaceCountCore)),
    ];

    cfg_if! {
        if #[cfg(any(target_arch = "x86", target_arch = "x86_64"))] {
            if is_x86_feature_detected!("ssse3") {
                impls.push(Box::new(MmapCounter::new(simd::x86::Ssse3Core::new(&[b' ', b'\n']))));
                impls.push(Box::new(MmapParallelCounter::new(simd::x86::Ssse3Core::new(&[
                    b' ', b'\n',
                ]))));
            } else {
                eprintln!("SSSE3 not available")
            }

            if is_x86_feature_detected!("avx2") {
                impls.push(Box::new(MmapCounter::new(simd::x86::Avx2Core::new(&[b' ', b'\n']))));
                impls.push(Box::new(MmapParallelCounter::new(simd::x86::Avx2Core::new(&[
                    b' ', b'\n',
                ]))));
            } else {
                eprintln!("AVX2 not available")
            }
        }
    }

    impls
}

fn main() -> Result<(), anyhow::Error> {
    let path = env::args()
        .nth(1)
        .map(path::PathBuf::from)
        .expect("Must provide a file to read as the first argument");

    {
        println!("Reading the input file to warm the VFS cache...");
        // read the file entirely first so the kernel vfs cache is warmed up for all runs equally
        let mut file = fs::File::open(&path)?;
        let mut buf = vec![0_u8; 10 * 1024];
        loop {
            if file.read(&mut buf[..])? == 0 {
                // EOF
                break;
            }
        }
    }

    // configure thread pool with physical, not logical, cores to avoid unpredictable HT effects
    let cpus = num_cpus::get_physical();
    println!("Using {} threads for parallel implementations", cpus);
    rayon::ThreadPoolBuilder::new()
        .num_threads(cpus)
        .build_global()
        .unwrap();

    let size = fs::metadata(&path)?.len();
    let units = &["B", "KiB", "MiB", "GiB"];

    let mut perf_counter = perf_counter::build_perf_counter();

    let flamegraph_dir = path::Path::new("tmp/flamegraphs");
    fs::create_dir_all(flamegraph_dir)?;
    // set permissions so that the results are easily usable even when run as root
    fs::set_permissions(flamegraph_dir, fs::Permissions::from_mode(0o755))?;

    let mut baseline = None;
    for counter in all_counters() {
        println!("{:?}", counter);

        let start = time::Instant::now();

        let pprof_guard = pprof::ProfilerGuard::new(100);

        perf_counter.reset().context("Failed to reset")?;
        perf_counter.start().context("Failed to start")?;
        let count = counter.count_words(&path)?;
        let elapsed = start.elapsed();
        perf_counter
            .stop(&elapsed, size)
            .context("Failed to stop")?;

        match pprof_guard.and_then(|g| g.report().build()) {
            Ok(report) => {
                let mut svg = flamegraph_dir.to_path_buf();
                svg.push(format!("{:?}.svg", counter));
                let file = fs::File::create(&svg)?;
                report.flamegraph(file)?;
                fs::set_permissions(&svg, fs::Permissions::from_mode(0o644))?;
            }
            Err(e) => eprintln!("Could not generate flamegraph: {:?}", e),
        };

        let speedup = baseline.get_or_insert(elapsed).as_secs_f64() / elapsed.as_secs_f64();

        let throughput = (size as f64) / (elapsed.as_secs_f64());

        // fold_first() isn't stable yet
        let thr_units = iter::repeat(throughput)
            .zip(units.iter())
            .enumerate()
            .map(|(index, (thr, &unit))| {
                // we'll run out of units before this overflows
                let divisor = 1024_u64.pow(index as u32) as f64;
                (thr / divisor, unit)
            })
            .collect::<Vec<_>>();

        let (scaled_throughput, unit) = thr_units
            .iter()
            .copied()
            .filter(|(thr, _unit)| *thr > 1.0)
            .last()
            .unwrap_or_else(|| thr_units[0]);

        println!(
            "\tcounted {} in {:?} ({:.2}x, {:.2} {}/s)",
            count, elapsed, speedup, scaled_throughput, unit
        );
    }

    Ok(())
}
