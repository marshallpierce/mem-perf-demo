use crate::mmap::MmapCore;
use custom_debug::Debug;

use std::arch::x86_64::{
    __m128i, __m256i, _mm256_and_si256, _mm256_cmpeq_epi8, _mm256_loadu_si256,
    _mm256_movemask_epi8, _mm256_or_si256, _mm256_set1_epi8, _mm256_set_m128i, _mm256_shuffle_epi8,
    _mm256_srli_epi16, _mm256_xor_si256, _mm_and_si128, _mm_cmpeq_epi8, _mm_loadu_si128,
    _mm_movemask_epi8, _mm_or_si128, _mm_set1_epi8, _mm_setr_epi8, _mm_shuffle_epi8,
    _mm_srli_epi16, _mm_xor_si128, _popcnt32,
};

/// Algorithm from http://0x80.pl/articles/simd-byte-lookup.html
#[derive(Debug)]
pub struct Ssse3Core {
    #[debug(skip)]
    lookup_0_7: __m128i,
    #[debug(skip)]
    lookup_8_15: __m128i,
    #[debug(skip)]
    bitmask_lookup: __m128i,
}

impl Ssse3Core {
    pub(crate) fn new(match_bytes: &[u8]) -> Self {
        let (lookup_0_7, lookup_8_15) = lookup_tables(match_bytes);

        // effectively lookup the bitmask based on hi_nibble % 8, hence the two sequences of 8
        // bytes: same result regardless of high bit in nibble.
        // Note setr_epi8, not set_epi8: first arg is lowest 8 bits, etc.
        let bitmask_lookup =
            unsafe { _mm_setr_epi8(1, 2, 4, 8, 16, 32, 64, -128, 1, 2, 4, 8, 16, 32, 64, -128) };

        Self {
            lookup_0_7,
            lookup_8_15,
            bitmask_lookup,
        }
    }

    fn ssse3_count(&self, bytes: &[u8]) -> u64 {
        let mut count = 0_u64;
        unsafe {
            for chunk in bytes.chunks(16) {
                let input = _mm_loadu_si128(chunk.as_ptr() as *const __m128i);
                // Mask maintains the high bit.
                // If the high nibble is > 7 (has the high bit set), then the high nibble should be
                // looked up in the 8_15 byte, not the 0_7 byte.
                // Conveniently, pshufb lookup with an index with the high bit set will always
                // produce 0 instead of looking up by the index.
                let indices_0_7 = _mm_and_si128(input, _mm_set1_epi8(0x8F_u8 as i8));
                // Analogously, we want to negate the high bit if it's set when looking up the 8_15
                // row: if hi_nibble < 8, set the high bit -> lookup produces 0, else unset it.
                let msb = _mm_and_si128(input, _mm_set1_epi8(0x80_u8 as i8));
                let indices_8_15 = _mm_xor_si128(indices_0_7, msb);

                // match row if high nibble < 7, else 0
                let row_0_7 = _mm_shuffle_epi8(self.lookup_0_7, indices_0_7);
                let row_8_15 = _mm_shuffle_epi8(self.lookup_8_15, indices_8_15);

                let higher_nibbles = _mm_and_si128(_mm_srli_epi16(input, 4), _mm_set1_epi8(0x0f));
                // get bitmasks with the (hi_nibble % 8)'th bit set
                let bitmask = _mm_shuffle_epi8(self.bitmask_lookup, higher_nibbles);

                // for each element, one row's byte will be zero
                let bitsets = _mm_or_si128(row_0_7, row_8_15);

                // for each element, either 0 bits (not a matching byte) or 1 bit (matching byte)
                // will be set
                let matching_bits = _mm_and_si128(bitsets, bitmask);

                // inflate the matching bits to 0xFF
                let matching_bytes = _mm_cmpeq_epi8(matching_bits, bitmask);

                // take the high bit from every byte (0xFF if matching, 0x00 otherwise)
                let matching_high_bits = _mm_movemask_epi8(matching_bytes);

                // we don't actually care about the bytes; we just want the count.
                let chunk_count = _popcnt32(matching_high_bits) as u64;
                count += chunk_count;
            }
        }

        count
    }
}

impl MmapCore for Ssse3Core {
    fn count_words(&self, bytes: &[u8]) -> u64 {
        // sse works on 16 bytes at a time
        let len_16byte = bytes.len() - (bytes.len() % 16);
        let sse_slice = &bytes[0..(len_16byte)];

        let mut count = self.ssse3_count(sse_slice);

        // deal with leftover bytes
        count += bytes[len_16byte..]
            .iter()
            .filter(|&&byte| byte == b' ' || byte == b'\n')
            .count() as u64;

        count
    }
}

#[derive(Debug)]
pub(crate) struct Avx2Core {
    #[debug(skip)]
    lookup_0_7: __m256i,
    #[debug(skip)]
    lookup_8_15: __m256i,
    #[debug(skip)]
    bitmask_lookup: __m256i,
}

impl Avx2Core {
    pub(crate) fn new(match_bytes: &[u8]) -> Avx2Core {
        // The __m128i's only are 16 bytes wide; we need 32 bytes.
        // AVX2 pshufb treats the lower 16 and upper 16 bytes of its 32 byte input independently,
        // so we just need to duplicate each lookup.
        let (lookup_0_7, lookup_8_15) = lookup_tables(match_bytes);
        let (lookup_0_7, lookup_8_15) = unsafe {
            (
                _mm256_set_m128i(lookup_0_7, lookup_0_7),
                _mm256_set_m128i(lookup_8_15, lookup_8_15),
            )
        };

        // effectively lookup the bitmask based on hi_nibble % 8, hence the two sequences of 8
        // bytes: same result regardless of high bit in nibble.
        // Note setr_epi8, not set_epi8: first arg is lowest 8 bits, etc.
        let bitmask_lookup =
            unsafe { _mm_setr_epi8(1, 2, 4, 8, 16, 32, 64, -128, 1, 2, 4, 8, 16, 32, 64, -128) };
        // as with match lookups above, double for 32 byte lanes
        let bitmask_lookup = unsafe { _mm256_set_m128i(bitmask_lookup, bitmask_lookup) };

        Avx2Core {
            lookup_0_7,
            lookup_8_15,
            bitmask_lookup,
        }
    }

    fn avx2_count(&self, bytes: &[u8]) -> u64 {
        assert_eq!(0, bytes.len() % 32);

        let mut count = 0_u64;

        unsafe {
            for chunk in bytes.chunks(32) {
                let input = _mm256_loadu_si256(chunk.as_ptr() as *const __m256i);
                // Mask maintains the high bit.
                // If the high nibble is > 7 (has the high bit set), then the high nibble should be
                // looked up in the 8_15 byte, not the 0_7 byte.
                // Conveniently, pshufb lookup with an index with the high bit set will always
                // produce 0 instead of looking up by the index.
                let indices_0_7 = _mm256_and_si256(input, _mm256_set1_epi8(0x8F_u8 as i8));
                // Analogously, we want to negate the high bit if it's set when looking up the 8_15
                // row: if hi_nibble < 8, set the high bit -> lookup produces 0, else unset it.
                let msb = _mm256_and_si256(input, _mm256_set1_epi8(0x80_u8 as i8));
                let indices_8_15 = _mm256_xor_si256(indices_0_7, msb);

                // match row if high nibble < 7, else 0
                let row_0_7 = _mm256_shuffle_epi8(self.lookup_0_7, indices_0_7);
                let row_8_15 = _mm256_shuffle_epi8(self.lookup_8_15, indices_8_15);

                let higher_nibbles =
                    _mm256_and_si256(_mm256_srli_epi16(input, 4), _mm256_set1_epi8(0x0f));
                // get bitmasks with the (hi_nibble % 8)'th bit set
                let bitmask = _mm256_shuffle_epi8(self.bitmask_lookup, higher_nibbles);

                // for each element, one row's byte will be zero
                let bitsets = _mm256_or_si256(row_0_7, row_8_15);

                // for each element, either 0 bits (not a matching byte) or 1 bit (matching byte)
                // will be set
                let matching_bits = _mm256_and_si256(bitsets, bitmask);

                // inflate the matching bits to 0xFF
                let matching_bytes = _mm256_cmpeq_epi8(matching_bits, bitmask);

                // take the high bit from every byte (0xFF if matching, 0x00 otherwise)
                let matching_high_bits = _mm256_movemask_epi8(matching_bytes);

                // we don't actually care about the bytes; we just want the count.
                let chunk_count = _popcnt32(matching_high_bits) as u64;
                count += chunk_count;
            }
        }

        count
    }
}

impl MmapCore for Avx2Core {
    fn count_words(&self, bytes: &[u8]) -> u64 {
        let lane_size = 32;
        let length_complete_lanes = bytes.len() - (bytes.len() % lane_size);
        let simd_slice = &bytes[0..(length_complete_lanes)];

        let mut count = self.avx2_count(simd_slice);

        // deal with leftover bytes
        count += bytes[length_complete_lanes..]
            .iter()
            .filter(|&&byte| byte == b' ' || byte == b'\n')
            .count() as u64;

        count
    }
}

/// Create lookup tables for byte matching.
/// Low nibble in a byte = index into these tables (4 bits -> lookup in 16 bytes)
/// The n'th bit in the resulting row of 16 bits (split into two 8 bit rows) is set if the
/// low nibble (index) and high nibble (n, the position in the row) are a byte we're
/// searching for.
/// The returned tuple is the first 8 bits and last 8 bits of each row, per byte, respectively.
fn lookup_tables(match_bytes: &[u8]) -> (__m128i, __m128i) {
    assert!(match_bytes.len() <= 16);

    let mut lookup_0_7 = [0x0_u8; 16];
    let mut lookup_8_15 = [0x0_u8; 16];

    for &byte in match_bytes.iter() {
        let lo_nibble = byte & 0x0F;
        let hi_nibble = (byte >> 4) & 0x0F;

        let bit = 1 << hi_nibble;
        if hi_nibble < 8 {
            lookup_0_7[lo_nibble as usize] |= bit;
        } else {
            lookup_8_15[lo_nibble as usize] |= bit;
        }
    }

    unsafe {
        (
            _mm_loadu_si128(lookup_0_7.as_ptr() as *const __m128i),
            _mm_loadu_si128(lookup_8_15.as_ptr() as *const __m128i),
        )
    }
}
