use std::{io, time};

/// A convenience layer above perfcnt's counters
pub trait PerfCounter {
    fn start(&mut self) -> Result<(), io::Error>;

    fn stop(&mut self, elapsed: &time::Duration, input_len: u64) -> Result<(), io::Error>;

    fn reset(&mut self) -> Result<(), io::Error>;
}

#[derive(Default)]
struct NoOpPerfCounter;

impl PerfCounter for NoOpPerfCounter {
    fn start(&mut self) -> Result<(), io::Error> {
        Ok(()) // no op
    }

    fn stop(&mut self, _elapsed: &time::Duration, _input_len: u64) -> Result<(), io::Error> {
        Ok(()) // no op
    }

    fn reset(&mut self) -> Result<(), io::Error> {
        Ok(()) // no op
    }
}

#[cfg(all(target_arch = "x86_64", target_os = "linux"))]
pub fn build_perf_counter() -> Box<dyn PerfCounter> {
    linux::LinuxPerfCounter::new()
        .map(|pc| {
            let dyn_box: Box<dyn PerfCounter> = Box::new(pc);
            dyn_box
        })
        .unwrap_or_else(|e| {
            eprintln!("Error while initializing perf counters; skipping: {:#}", e);
            Box::new(NoOpPerfCounter)
        })
}

#[cfg(not(all(target_arch = "x86_64", target_os = "linux")))]
pub fn build_perf_counter() -> Box<dyn PerfCounter> {
    eprintln!("Skipping perf counters; not on x64 Linux");
    Box::new(NoOpPerfCounter::default())
}

#[cfg(all(target_arch = "x86_64", target_os = "linux"))]
mod linux {
    use crate::perf_counter::PerfCounter;
    use anyhow::Context as _;
    use perfcnt::{linux, AbstractPerfCounter as _};
    use std::{io, time};

    pub struct LinuxPerfCounter {
        hw_events: HwEvents,
        cache_counters: CacheCounters,
    }

    impl LinuxPerfCounter {
        pub fn new() -> Result<Self, anyhow::Error> {
            let hw_events = HwEvents {
                cycles: linux::PerfCounterBuilderLinux::from_hardware_event(
                    linux::HardwareEventType::RefCPUCycles,
                )
                .finish()
                .context("Couldn't get cycles counter")?,
                instructions: linux::PerfCounterBuilderLinux::from_hardware_event(
                    linux::HardwareEventType::Instructions,
                )
                .finish()
                .context("Couldn't get instructions counter")?,
                branch_instr: linux::PerfCounterBuilderLinux::from_hardware_event(
                    linux::HardwareEventType::BranchInstructions,
                )
                .finish()
                .context("Couldn't get branch counter")?,
                branch_misses: linux::PerfCounterBuilderLinux::from_hardware_event(
                    linux::HardwareEventType::BranchMisses,
                )
                .finish()
                .context("Couldn't get branch miss counter")?,
            };

            let cache_counters = CacheCounters::new()?;

            Ok(LinuxPerfCounter {
                hw_events,
                cache_counters,
            })
        }
    }

    impl super::PerfCounter for LinuxPerfCounter {
        fn start(&mut self) -> Result<(), io::Error> {
            self.hw_events.start()?;
            self.cache_counters.start()?;

            Ok(())
        }

        fn stop(&mut self, elapsed: &time::Duration, input_len: u64) -> Result<(), io::Error> {
            self.hw_events.stop(elapsed, input_len)?;
            self.cache_counters.stop(elapsed, input_len)?;

            Ok(())
        }

        fn reset(&mut self) -> Result<(), io::Error> {
            self.hw_events.reset()?;
            self.cache_counters.reset()?;

            Ok(())
        }
    }

    struct HwEvents {
        cycles: linux::PerfCounter,
        instructions: linux::PerfCounter,
        branch_instr: linux::PerfCounter,
        branch_misses: linux::PerfCounter,
    }

    impl PerfCounter for HwEvents {
        fn start(&mut self) -> Result<(), io::Error> {
            self.cycles.start()?;
            self.instructions.start()?;
            self.branch_instr.start()?;
            self.branch_misses.start()?;
            Ok(())
        }

        fn stop(&mut self, elapsed: &time::Duration, input_len: u64) -> Result<(), io::Error> {
            self.cycles.stop()?;
            self.instructions.stop()?;
            self.branch_instr.stop()?;
            self.branch_misses.stop()?;

            let cycles = self.cycles.read()?;
            let instr = self.instructions.read()?;
            let br_instr = self.branch_instr.read()?;
            let br_miss = self.branch_misses.read()?;

            println!(
                "\t{:.2} Mcycles ({:.2} Mcy/s, {:.2} cy/byte)",
                cycles as f64 / 1_000_000_f64,
                cycles as f64 / 1_000_000_f64 / elapsed.as_secs_f64(),
                cycles as f64 / input_len as f64,
            );
            println!(
                "\t{:.2} Minstr ({:.2} instr/byte, {:.2} instr/cy)",
                instr as f64 / 1_000_000_f64,
                instr as f64 / input_len as f64,
                instr as f64 / cycles as f64,
            );
            println!(
                "\t{:.2} Mbranches ({:.2}% miss)",
                br_instr as f64 / 1_000_000_f64,
                br_miss as f64 / br_instr as f64
            );

            Ok(())
        }

        fn reset(&mut self) -> Result<(), io::Error> {
            self.cycles.reset()?;
            self.instructions.reset()?;
            self.branch_instr.reset()?;
            self.branch_misses.reset()?;
            Ok(())
        }
    }

    struct CacheCounters {
        counters: Vec<(&'static str, ReadCounters)>,
    }

    impl CacheCounters {
        fn new() -> Result<CacheCounters, io::Error> {
            Ok(CacheCounters {
                counters: vec![
                    ("L1 data", ReadCounters::from(linux::CacheId::L1D)?),
                    ("Last lvl", ReadCounters::from(linux::CacheId::LL)?),
                    ("dTLB", ReadCounters::from(linux::CacheId::DTLB)?),
                    ("Branch pred", ReadCounters::from(linux::CacheId::BPU)?),
                ],
            })
        }
    }

    impl PerfCounter for CacheCounters {
        fn start(&mut self) -> Result<(), io::Error> {
            for (_, c) in self.counters.iter_mut() {
                c.start()?;
            }
            Ok(())
        }

        fn stop(&mut self, elapsed: &time::Duration, input_len: u64) -> Result<(), io::Error> {
            for (label, c) in self.counters.iter_mut() {
                c.stop(elapsed, input_len)?;

                let (access, miss) = c.read()?;

                println!(
                    "\t{}: {:.2} Maccesses, {:.2}% miss",
                    label,
                    access as f64 / 1_000_000_f64,
                    miss as f64 / access as f64
                );
            }
            Ok(())
        }

        fn reset(&mut self) -> Result<(), io::Error> {
            for (_, c) in self.counters.iter_mut() {
                c.reset()?;
            }
            Ok(())
        }
    }

    struct ReadCounters {
        access: linux::PerfCounter,
        miss: linux::PerfCounter,
    }

    impl PerfCounter for ReadCounters {
        fn start(&mut self) -> Result<(), io::Error> {
            self.access.start()?;
            self.miss.start()?;
            Ok(())
        }

        fn stop(&mut self, _elapsed: &time::Duration, _input_len: u64) -> Result<(), io::Error> {
            self.access.stop()?;
            self.miss.stop()?;
            Ok(())
        }

        fn reset(&mut self) -> Result<(), io::Error> {
            self.access.reset()?;
            self.miss.reset()?;
            Ok(())
        }
    }

    impl ReadCounters {
        fn from(cache_id: linux::CacheId) -> Result<ReadCounters, io::Error> {
            Ok(ReadCounters {
                access: linux::PerfCounterBuilderLinux::from_cache_event(
                    cache_id,
                    linux::CacheOpId::Read,
                    linux::CacheOpResultId::Access,
                )
                .finish()?,
                miss: linux::PerfCounterBuilderLinux::from_cache_event(
                    cache_id,
                    linux::CacheOpId::Read,
                    linux::CacheOpResultId::Miss,
                )
                .finish()?,
            })
        }

        fn read(&mut self) -> Result<(u64, u64), io::Error> {
            Ok((self.access.read()?, self.miss.read()?))
        }
    }
}
