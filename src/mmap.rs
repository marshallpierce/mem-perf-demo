use crate::WordCounter;

use rayon::iter::{ParallelBridge, ParallelIterator};
use std::{fmt::Debug, fs, io, path};

/// Reads bytes via a memory mapped file -- no copy from kernel to userspace.
///
/// Delegates to [MmapCore] since the mmap bits are always the same no matter how counting is done.
#[derive(Debug)]
pub(crate) struct MmapCounter<T: MmapCore> {
    core: T,
}

impl<T: MmapCore> MmapCounter<T> {
    pub(crate) fn new(core: T) -> MmapCounter<T> {
        MmapCounter { core }
    }
}

impl<T: MmapCore> WordCounter for MmapCounter<T> {
    fn count_words(&self, path: &path::Path) -> io::Result<u64> {
        let file = fs::File::open(path)?;
        // mmaping large files is slow on macOs
        // https://developer.apple.com/library/archive/documentation/FileManagement/Conceptual/FileSystemAdvancedPT/MappingFilesIntoMemory/MappingFilesIntoMemory.html#//apple_ref/doc/uid/TP40010765-CH2-SW1
        // TODO huge page support
        let mmap = unsafe { memmap::MmapOptions::new().map(&file)? };

        let count = self.core.count_words(&mmap[..]);

        Ok(count)
    }
}

/// A multithreaded version of [MmapCounter].
#[derive(Debug)]
pub(crate) struct MmapParallelCounter<T: MmapCore> {
    core: T,
}

impl<T: MmapCore> MmapParallelCounter<T> {
    pub(crate) fn new(core: T) -> MmapParallelCounter<T> {
        MmapParallelCounter { core }
    }
}

impl<T: MmapCore> WordCounter for MmapParallelCounter<T> {
    fn count_words(&self, path: &path::Path) -> io::Result<u64> {
        let file = fs::File::open(path)?;
        let mmap = unsafe { memmap::MmapOptions::new().map(&file)? };

        let count = mmap[..]
            // 256 MiB chunks
            .chunks(256 * 1024 * 1024)
            // hand each chunk off to a thread pool
            .par_bridge()
            .map(|chunk| self.core.count_words(chunk))
            .sum();

        Ok(count)
    }
}

/// The actual counting part of an mmap-based counter
pub(crate) trait MmapCore: Debug + Sync {
    fn count_words(&self, bytes: &[u8]) -> u64;
}

/// Like [crate::io_read::VecFilterCountCounter], `filter()`s bytes with the obvious predicate, with `count()`.
#[derive(Debug)]
pub(crate) struct ByteIterFilterCountCore;

impl MmapCore for ByteIterFilterCountCore {
    fn count_words(&self, bytes: &[u8]) -> u64 {
        bytes
            .iter()
            .filter(|&&byte| byte == b' ' || byte == b'\n')
            .count() as u64
    }
}

/// Also uses filter/count but with more bytes, showing that this approach slows down when
/// matching on more bytes
#[derive(Debug)]
pub(crate) struct ByteIterFilterMoreWhitespaceCountCore;

impl MmapCore for ByteIterFilterMoreWhitespaceCountCore {
    fn count_words(&self, bytes: &[u8]) -> u64 {
        bytes
            .iter()
            .filter(|&&byte| {
                byte == b' ' || byte == b'\n' || byte == b'\t' || byte == b'\r' || byte == 0xB
            })
            .count() as u64
    }
}
