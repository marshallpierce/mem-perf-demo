Demo of solving the same problem a few different ways to show the effects of memory copies and access patterns.

To calculate the word count in a file with a variety of implementations:

```
cargo run --release path/to/file
```

# Perf counters

Perf counters will only work on Linux x64, and require the nightly toolchain (`rustup toolchain add nightly`, then `cargo +nightly ...`).

It also requires access to more perf counters than are typically available on a virtualized instance, so if you see errors about "No such file", that's `perf_event_open` failing to find the necessary counters.

# Development

For development convenience on non-Linux systems, start a shell in a Linux box with the rust toolchain
via `docker-compose run rust`.
